# CHANGELOG

## 2024-11-07

- #20 Updated `Package-Update` script to include option to restart services only

## 2024-04-16

- #19 Added Windows `WSL` Shutdown Playbook

## 2024-04-01

- #18 Added `WOL` Etherwake Playbook

## 2024-03-27

- #17 Added extra and adhoc directories to gitignore

## 2024-01-06

- #15 Issue closed as out of scope

## 2023-10-31

- #13 Added new `Ara Docker Update` script to update the Ara docker containers

## 2023-09-26

- #12 Updated `Package-Update` script to only restart services when the package is updated

## 2023-09-09

- #10 Fixed Bug with SSL Impacting Portainer Server/Agent Communication (introduced SSL as an optional variable as a result)

## 2023-09-08

- #11 Reworked Repository Structure and Updated Documentation

## 2023-08-07

- #9 Updated Git Scripts To Allow for HTTPS and SSH Git URLs

## 2023-08-01

- #8 Updated READMEs to Include Encryption Notes

## 2023-07-28

- #7 Updated Cloudflared Script to Become Generalised Package Update Script

## 2023-07-26

- #6 Updated Portainer Script to Include Updating Agent and Edge Agents

## 2023-07-23

- #5 Added example.inventory.ini file and Updated README.md with Variables

## 2023-07-20

- #4 Added Cloudflared Update Script

## 2023-07-19

- #3 Added Ara and Script Repository Git Update Script

## 2023-07-15

- #2 Added Portainer Update Script
- #1 Setup Directory Structure
