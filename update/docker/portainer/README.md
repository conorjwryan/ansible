# Portainer Update Script

This script will update the Portainer-CE, Portainer-Agent and Portainer-EdgeAgent docker containers to the latest version.

## Requirements

The script requires installing the docker community collection. This can be done by running the following command:

```bash
ansible-galaxy collection install community.docker
```

## A note about SSL Certificates and Keys

If you've set up Portainer-CE server to work over `SSL` you will need to create a directory containing the `portainer-cert.pem` and `portainer-key.pem` files in order for this script to work. The `portainer-cert.pem` file should contain the SSL certificate and the `portainer-key.pem` file should contain the SSL key.

The directory can be placed anywhere on the computer but it is recommended to place it in the same directory as other portainer related files like:

```bash
/home/user/docker/portainer/ssl
```

You have to specifically define the `portainer_ssl_dir` variable in the computers variables section with the location of the ssl directory on the system.

If you do not want to enable SSL you can just leave the variable undefined.

### I've already added SSL to Portainer-CE over the web interface

If you've previously added SSL to Portainer-CE over the web interface there is a good chance you already have these files. They are located in the `ssl` directory in the Portainer-CE data directory. The default location for the data directory is `/var/lib/docker/volumes/portainer_data/_data` but this may be different depending on your system.

If you have these files you can just copy them to the directory you defined in the `portainer_ssl_dir` variable. You will need to rename them to `portainer-cert.pem` and `portainer-key.pem` respectively.

## Host File Setup

In starting your host file, first you will need to define the individual computers you want to connect to as well as the `sudo` password for each computer if required by docker (See [here](#docker--sudo) for more information).

### Variables

Here is a list of the variables that can be defined in the host file specifically for this script:

#### Computer Variables

These need to be defined in the specific host vars section:

| Variable | Description |
| --- | --- |
| `ansible_become_password` | The `sudo` password for the computer if required by docker. |
| `portainer_ssl_dir` | The directory containing the `portainer-cert.pem` and `portainer-key.pem` files (Required if SSL is to be enabled - assumes already have signed certificates generated). |

#### Group Variables

These variables need to be defined in the group vars section for each portainer type (server, agent, edge-agent) for the script to work:

| Variable | Description |
| `portainer_container` | The name of the Portainer docker container. |
| `portainer_image` | The name of the Portainer docker image. |
| `portainer_port` | The port that Portainer is running on. |
| `portainer_data` | The name of the Portainer data directory. |

The `portainer_edge_agent` group is not required if you are not using edge agents (or the same applies if you are not using `standard agents`). If you are using edge agents you will need to define the variables for the `portainer_edge_agent` group as well.

### Example

In the following example I have two servers running Portainer-CE, one of which has SSL enabled, and two other servers running Portainer-Agent.

I have included the `portainer_edge_agent` group as well but it is commented out as I am not using edge agents.

```ini
[example_server_1]
example_server_1

[example_server_1:vars]
ansible_become_password=example_password
portainer_ssl_dir=/home/user/docker/portainer/ssl

[example_server_2]
example_server_2

[example_server_2:vars]
ansible_become_password=PassW0rd


[example_agent_1]
example_agent_1

[example_agent_1:vars]
ansible_become_password=ex_pass

[example_agent_2]
example_agent_2

[example_agent_2:vars]
ansible_become_password=password_example

; [example_edge_agent_1]
; example_edge_agent_1

; [example_edge_agent_1:vars]
; ansible_become_password=example_password

```

Most of the variables in this script are defined in the computer groups rather than on the individual computers. This is because the script updates the server and agents at the same time.

As a result of this, the computers above need to be grouped into one of three potential groups:

- `portainer_server`,
- `portainer_agent`,
- `portainer_edge_agent`.

The `portainer_server` group will contain all the computers running the Portainer-CE server. The `portainer_agent` group will contain all the computers running the Portainer-Agent. The `portainer_edge_agent` group will contain all the computers running the Portainer-Edge-Agent.

```ini
[portainer_server:children]
example_server_1
example_server_2

[portainer_server:vars]
portainer_container=portainer
portainer_image=portainer/portainer-ce:latest
portainer_port="9443:9443"
portainer_data=portainer_data

[portainer_agent:children]
example_agent_1
example_agent_2

[portainer_agent:vars]
portainer_container=portainer_agent
portainer_image=portainer/agent:latest
portainer_port="9001:9001"
portainer_data=portainer_agent_data

; [portainer_edge_agent:children]
; example_edge_agent_1

; [portainer_edge_agent:vars]
; portainer_container=portainer_edge_agent
; portainer_image=portainer/agent:latest
; portainer_port="8001:8001"
; portainer_data=portainer_edge_agent_data

[portainer:children]
portainer_server
portainer_agent
; portainer_edge_agent


```

The subgroups `portainer_server` and `portainer_agent` are defined first before the general portainer group. This ensures that both the server and agents can be updated in the same group using different values for the variables.

If you wanted to include `edge agents` you would just need to uncomment the above code and add the `example_edge_agent_1` computer to the `portainer_edge_agent` group. You would also need to uncomment the `portainer_edge_agent` group in the `portainer` group.

If you do not want to update the `portainer_(edge_)agent` group (or have none) you can just remove it from the `portainer` group.

## Running the Script

Once you have set up your host file you can run the script using the following command:

```bash
ansible-playbook -i hosts update/docker/portainer-update.yml
```

The above example assumes that your hosts file is called `hosts` you are running it from the root directory of this repository.

If you are running it from a different directory or your hosts file is called something else you will need to change the command accordingly.

### Running the Script with an Encrypted Host File (Recommended)

It is recommended to encrypt your host file if it contains sensitive information like passwords. This can be done using the following command:

```bash
ansible-vault encrypt hosts
```

This will encrypt the file using the password you provide. You will need to provide this password every time you run the script.

To run the script with an encrypted host file you will need to add the `--ask-vault-pass` flag to the command:

```bash
ansible-playbook -i hosts update/docker/portainer-update.yml --ask-vault-pass
```

To edit the encrypted host file you will need to use the following command:

```bash
ansible-vault edit hosts
```

Or if you want to decrypt the file:

```bash
ansible-vault decrypt hosts
```

## Notes

### Portainer Data on Agent

The `portainer_data` variable is pointless for the `portainer_agent` group as the agent does not store any data but is required for the script to function.

### Docker / Sudo

Docker requires `sudo` privileges to run on Linux by default but can be configured to run without root privileges.

If you have configured your system to allow docker to run without `sudo` privileges you can remove the `ansible_become_password` variable from the computer.

It is recommended to include it however if you are running other scripts in this repository as they may require `sudo` privileges.

## Limitations

- It only works with "Docker Standalone" installations on **Linux only** Systems
- It has not been tested to work work with `Docker Swarm` or `Docker Desktop`
- Currently it only works with `Portainer-CE` and not `Portainer-Business`
- Currently the only way to enable `Portainer Server` with SSL is to set `portainer_ssl_dir` to a directory containing the specifically named `portainer-cert.pem` and `portainer-key.pem` files. It will not work with other named files.
