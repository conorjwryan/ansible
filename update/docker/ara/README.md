# Ara Update Script

This script will call the `bash` update script on remote systems with Ara Dockerised WordPress installed to update both the `git` repository and the `docker` containers.

## Requirements

This script assumes that you already have a `Project Ara` Installation already configured and working on the remote system. If you do not have this already set up please follow the instructions in the [Project Ara Repository](https://gitlab.com/conorjwryan/ara).

### How This Script Works

Unlike other scripts in this repository this script does not use traditional Ansible Modules to update the remote system. Instead it uses the `command` module to call a `bash` script on the remote system. This is because `docker compose V2` does not have direct Ansible support.

As a result, it is recommended that you test the `bash` script on a single remote system before using this script. You can find the `bash` script in the `cli/update` directory of the [Project Ara Repository](https://gitlab.com/conorjwryan/ara/).

## Host File Setup

To ensure the script works you need to first define the computers and associated variables at the top of the file.

In the following example I have 3 computers running the Ara project. Localhost is running the project in a development environment and the other two computers are running the project in a production environment.

```ini
[localhost]
127.0.0.1

[localhost:vars]
ara_dest_paths=/Users/conor/Sites/ara

[example_computer_1]
example_computer_1

[example_computer_1:vars]
; single ara repository
ara_dest_paths=/home/user/ara

[example_computer_2]
example_computer_2

[example_computer_2:vars]
; multiple ara repositories
ara_dest_paths=/home/user/ara,/home/user/ara2
```

Key to the above example is that each computer has a `ara_dest_paths` variable defined. This variable defines the location of the `ara` directory on the remote system. This is used to ensure that the correct `ara` directory is updated.

Now that we have defined the computers and associated variables we need to define the computer groups. The following groups need to be defined below:

```ini
[ara_update:children]
example_computer_1
example_computer_2
```

You'll see in the above example that `localhost` is not included in the `ara_update` group. This is because the `localhost` computer is a `development` environment and is not setup fully. It also symbolises the computer which you run the ansible script on.

For more information on how multiple scripts in this repository interact with a single hosts file, please see the `example-hosts.ini` file in the root of this repository.

## Running the Script

Once you have set up your host file you can run the script using the following command:

```bash
ansible-playbook update/docker/ara/docker-ara-update.yml -i hosts.ini
```

The above example assumes that your hosts file is called `hosts` you are running it from the root directory of this repository.

If you are running it from a different directory or your hosts file is called something else you will need to change the command accordingly.

## Related Scripts

This script is part of a set which can be used to update all the components of a Project Ara installation. The other scripts are:

### Ara Git Repository Update

- [Ara Git Repository Update](https://gitlab.com/conorjwryan/ansible/update/git/ara/)

It is recommended that you set up the Ara Git Repository Update script before using this script as it shares common variables. It also is a way to safely update the repository before updating the docker containers.

### Ara Update Bash Script

- [Ara Update Bash Script](https://gitlab.com/conorjwryan/ara/cli/update/)

If you would like to learn more about the script which this ansible playbook calls on the remote system go to the above link. The bash script offers finer control over the update process and it is recommended that you test that first before using this ansible playbook.
