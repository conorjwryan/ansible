# Git Updater Scripts

These Ansible scripts will synchronise git repositories across remote hosts.

Both scripts require variables to be defined in the individual computers and groups in the hosts file.

This is to allow for different synchronisation methods (HTTPS or SSH) to be used on individual computers. For example HTTPS could be used for computers that pull commits from a public repository which do not require push access.

SSH could be used for development computers which require push access in a public repository or pull access in a private repository.

## List of Scripts

Scripts are as follows:

### Git Ara Pull

This script synchronises the [Ara](https://gitlab.com/conorjwryan/ara) git repository across remote hosts. More information can be found in the [Git Ara Pull README](update/git/ara/README.md).

### Git Scripts Pull

This script synchronises the [Scripts](https://gitlab.com/conorjwryan/scripts) git repository across remote hosts. More information can be found in the [Git Scripts Pull README](update/git/scripts/README.md).
