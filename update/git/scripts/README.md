# Scripts Git Repository Pull

This script synchronises the git Bash Script repository, found [here](https://gitlab.com/conorjwryan/scripts), across multiple servers.

## How this Script Works

Using Ansible this script does the following:

1. Checks all the computers defined in the `git_script` group in the hosts file to see if their git repositories have modified files - if it does it will skip step 2.
2. If no modified files are found then the latest version of the git repository is pulled to the server.

## Host File Setup

To ensure the script works you need to first define the computers and associated variables at the top of the file.

In the following example I have 3 computers running the Bash Script project. Localhost is running the project in a development environment and the other two computers are running the project in a production environment. In this example I am using SSH on my development computer and HTTPS on the production computers.

```ini
[localhost]
127.0.0.1

[localhost:vars]
script_dest_paths=/Users/conor/Sites/ara

[example_computer_1]
example_computer_1

[example_computer_1:vars]
script_dest_paths=/home/user/ara

[example_computer_2]
example_computer_2

[example_computer_2:vars]
script_dest_paths=/home/user/ara,/home/user/ara2
```

Now that we have defined the computers and associated variables we need to define the computer groups. The following groups need to be defined: `git_script_https`, `git_script_ssh` and `git_script`.

The `git_script_https` group is used to define the computers that use HTTPS to synchronise the git repository. The `git_script_ssh` group is used to define the computers that use SSH to synchronise the git repository. The `git_script` group is used to define the computers that use both HTTPS and SSH to synchronise the git repository.

For this script to work only the `git_script_https` and `git_script_ssh` groups need to be changed. The `git_script` group calls the other two groups so it does not need to be changed unless you are exclusively using HTTPS or SSH only.

```ini
[git_script_https:children]
example_computer_1
example_computer_2

[git_script_https:vars]
script_repo=https://gitlab.com/conorjwryan/scripts.git

[git_script_ssh:children]
localhost

[git_script_ssh:vars]
script_repo=git@gitlab.com:conorjwryan/scripts.git

[git_script:children]
git_script_https
git_script_ssh
```

For more information on how multiple scripts in this repository interact with a single hosts file, please see the `example-hosts.ini` file in the root of this repository.
