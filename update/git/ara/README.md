# Ara Git Repository Pull

## About

`Ara Dockerised WordPress` (Project Ara) is a platform agnostic WordPress production environment that uses Docker. It is designed to be quick and easy to setup with minimal configuration complete with a reverse proxy, caching and SSL out of the box (through the help of CloudFlare)

This script is used to pull the latest version of the `Ara Dockerised WordPress` git repository, to multiple remote servers defined in an inventory file.

## How this Script Works

Using Ansible this script does the following:

1. Checks all the computers defined in the `git_ara` group in the hosts file to see if their git repositories have modified files - if it does it will skip step 2.
2. If no modified files are found then the latest version of the git repository is pulled to the server.

## Host File Setup

To ensure the script works you need to first define the computers and associated variables at the top of the file.

In the following example I have 3 computers running the Ara project. Localhost is running the project in a development environment and the other two computers are running the project in a production environment. In this example I am using SSH on my development computer and HTTPS on the production computers.

```ini

[localhost]
127.0.0.1

[localhost:vars]
ara_dest_paths=/Users/conor/Sites/ara

[example_computer_1]
example_computer_1

[example_computer_1:vars]
; single ara repository
ara_dest_paths=/home/user/ara

[example_computer_2]
example_computer_2

[example_computer_2:vars]
; multiple ara repositories
ara_dest_paths=/home/user/ara,/home/user/ara2
```

Now that we have defined the computers and associated variables we need to define the computer groups. The following groups need to be defined: `git_ara_https`, `git_ara_ssh` and `git_ara`.

The `git_ara_https` group is used to define the computers that use HTTPS to synchronise the git repository. The `git_ara_ssh` group is used to define the computers that use SSH to synchronise the git repository. The `git_ara` group is used to define the computers that use both HTTPS and SSH to synchronise the git repository.

For this script to work only the `git_ara_https` and `git_ara_ssh` groups need to be changed. The `git_ara` group calls the other two groups so it does not need to be changed unless you are exclusively using HTTPS or SSH only.

```ini
[git_ara_https:children]
example_computer_1
example_computer_2

[git_ara_https:vars]
ara_repo=https://gitlab.com/conorjwryan/ara.git

[git_ara_ssh:children]
localhost

[git_ara_ssh:vars]
ara_repo=git@gitlab.com:conorjwryan/ara.git

[git_ara:children]
git_ara_https
git_ara_ssh
```

For more information on how multiple scripts in this repository interact with a single hosts file, please see the `example-hosts.ini` file in the root of this repository.

## Running the Script

Once you have set up your host file you can run the script using the following command:

```bash
ansible-playbook -i hosts update/git/ara/git-ara-pull.yml
```

The above example assumes that your hosts file is called `hosts` you are running it from the root directory of this repository.

If you are running it from a different directory or your hosts file is called something else you will need to change the command accordingly.

## Related Scripts

This script is part of a set which can be used to update all the components of a Project Ara installation. The other scripts are:

- [Ara Docker Update](https://gitlab.com/conorjwryan/ansible/update/docker/ara/)

The Ara Docker Update Script is more complex than this script. It includes the ability to update the git repository and the docker containers.
