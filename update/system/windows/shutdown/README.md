# Windows Shutdown

This script uses to remotely shutdown a Windows machine on the network using WSL. This script will not run on the Windows machine itself.

## Prerequisites

* This script connects to the Windows machine WSL distribution to perform this command so you need to have the WSL distribution installed on the Windows machine.
  * Follow this [guide](https://medium.com/@wuzhenquan/windows-and-wsl-2-setup-for-ssh-remote-access-013955b2f421) on how to setup `SSH` on `WSL` to forward `SSH` between `WSL` and `Windows` host.

## Host File Setup

```ini
[example_computer_1]
192.168.0.2

There are no variables to set for this script.
```

## Running the Playbook

```bash
ansible-playbook --ask-vault-pass -i hosts update/system/windows/shutdown/shutdown.yml --extra-vars="HOST=example_computer_1 TIME=30"
```

The above command assumes that you are running the command from the root of the repository.

## Notes

### Playbook Process

The script will send the `shutdown` command to the computer to shut it down by calling the shutdown.exe directly via the `C:\Windows\System32` path. The script will wait for 15 seconds before shutting down the computer.

In order to test the script, you can run the following command via `WSL` `ssh:

```bash
/mnt/c/Windows/System32/shutdown.exe /s /f /t 15
```

### Error: HOST Variable Must Be Defined

In order to run the script, you need to define the following variables in the `ansible-playbook` command where `HOST` has to be defined otherwise the script will not run.

```bash
ansible-playbook --ask-vault-pass -i hosts update/system/windows/shutdown/shutdown.yml --extra-vars="HOST=example_computer_1"
```

### Limitations / Considerations

* You need to have the correct permissions to shutdown the computer.
* Without the `TIME` variable set the script computer waits 15 seconds before shutting down.
* It will fail if the `WSL VM` is not running on the Windows machine.
