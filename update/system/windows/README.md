# Windows Related Playbooks

This directory contains playbooks that are related to Windows systems.

`Ansible` and `Windows` don't work well natively together (yet) so these playbooks are designed to control remote `Windows` machines from a `WSL` (Windows Subsystem for Linux) distribution.

## Playbooks

* [Windows Shutdown](shutdown/README.md) - Remotely shutdown a `Windows` machine on the network using `WSL`.
