# Specific Package Update

This script will update server packages on remote hosts to the latest version.

## Host File Setup

The following variables need to be defined in computers in the hosts file:

```ini
[example_computer_1]
example_computer_1

[example_computer_1:vars]
package_update_name=cloudflared, tailscale
package_update_service=cloudflared, tailscaled

[example_computer_2]
example_computer_2

[example_computer_2:vars]
package_update_name=cloudflared
package_update_service=cloudflared
```

Each computer can have a different list of packages to update.

In the above example that the `package_update_name` and `package_update_service` variables are defined as comma separated lists.

## Running the Script

Even after the `hosts` file is set up, the script will error out. Variables also need to be put into the command line in order to run the script. The following variables need to be defined:

### Command Line Variables

- `HOST` - this is the name of the group as defined in the `hosts` file. It can either be `package_update` or any other group name defined in the `hosts` file.
- `RESTART_ONLY` (OPTIONAL) - Set this to `true` if you only want to restart the service and not update the package. If this is not set, the script will update the package and restart the service after.

### Usage

To run the script in default mode, use the following command:

```bash
ansible-playbook -i hosts specific-package-update.yml --extra-vars "HOST=package_update"
```

To run the script in restart only mode, use the following command:

```bash
ansible-playbook -i hosts specific-package-update.yml --extra-vars "HOST=package_update RESTART_ONLY=true"
```

## Notes

### Package Name and Service Name May Be Different

The name of the service and the associated service can be different as in the case of `tailscale` and `tailscaled`. Check the package documentation to make sure the service name is correct and thus does not produce an error. If the service name is not defined the script will not restart the service.

### When Updates Are Skipped

The script will not update the package if it is already on the latest version, as determined by the package manager. It also only refreshes the cache every hour, so if the package script was run within the last hour it will not update the package, even if a new update has been released in that time.

## Limitations and Considerations

- It only works on **Linux only**. This does not work on Windows or MacOS.
- It has only been tested on Ubuntu 22.04 systems but should work on other non-Debian based systems as well (because it uses Ansible's agnostic `package` command) - just keep in mind the package/service name may be different
