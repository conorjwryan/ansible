# Etherwake

This script uses the `etherwake` command on a Linux machine to wake up a Windows machine on the network. Crucially with this playbook you can specify the interface the packet is sent on.

## Rational

My setup is as follows:

- I have my development machine on subnet `A` and my `Windows` machine on subnet `B`.
- I cannot directly send a `Wake On LAN` packet from my development machine to my `Windows` machine because they are on different subnets.
- I have a `Linux` machine with two `NICs`. One interface is on Subnet `A` and the other is on subnet `B`.
- I can send a `Wake On LAN` packet from my `Linux` machine to my `Windows` machine because they are on the same subnet.

## Host File Setup

```ini
[example_computer_1]
192.168.0.2

[example_computer_1:vars]
# Required variables
ew_interface=wlan0 # NIC interface to send the packet on
ew_mac=00:00:00:00:00:00 # MAC of the computer to wake up

# Optional variables
ew_hostname=example_computer_1 # Name of the computer to wake up
ew_ip=192.168.50.6 # IP of the computer to wake up
```

## Running the Playbook

```bash
ansible-playbook --ask-vault-pass -i hosts update/system/linux/etherwake/etherwake.yml --extra-vars="HOST=example_computer_1 SKIP_CHECK=yes"
```

The above command assumes that you are running the command from the root of the repository.

## Notes

### Playbook Process

The process of the playbook is as follows:

1. The script will check if the computer has `etherwake` is installed on the computer. (OPTIONAL)

    If you want to skip the check, you can set `SKIP_CHECK` to `true` in the `ansible-playbook` command.

2. The script will then send the `etherwake` command to the computer to wake it up.

### HOST Variable Must Be Defined

In order to run the script, you need to define the following variables in the `ansible-playbook` command where `HOST` has to be defined otherwise the script will not run.

```bash
ansible-playbook -i hosts etherwake.yml --extra-vars "HOST=example_computer_1"
```

The `HOST` refers to the computer you want to run the `etherwake` command on NOT the one you want to wake up.

### Playbook Needlessly Checks for `etherwake` / Takes Too Long

The playbook will check if `etherwake` is installed on the computer. This is not necessary and can be skipped by setting `SKIP_CHECK` to `true` in the `ansible-playbook` command.

```bash
ansible-playbook -i hosts etherwake.yml --extra-vars "HOST=example_computer_1 SKIP_CHECK=true"
```

Any value can be used for `SKIP_CHECK` as long as it is not `false`.

### Limitations / Considerations

- This script has only been tested to work on a remote `Linux` machine to wake up a `Windows` machine.
- You cannot currently choose the port to send the `etherwake` packet on.
- At the moment, the script can only wake up one computer per run. To wake up other computers you will need to run the script again with the appropriate `HOST` variable.
