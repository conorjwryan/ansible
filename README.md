# Ansible Scripts

Ansible is a tool for automating tasks on remote hosts. It and uses SSH to connect to remote hosts and runs scripts (called playbooks) across multiple hosts at once saving time and effort. This is a repository for my Ansible Scripts which help with automation of repetitive tasks.

I have crafted these scripts so they can be used by anyone with a basic understanding of Ansible on any number of remote hosts. Below is a brief description what of each script does and its limitations.

## Note about Windows Support

These scripts are used to update packages and synchronise git repositories / docker containers across remote hosts. Currently Windows support is limited to select scripts due to the way Ansible interacts with that operating system. A workaround for this is to use the Windows Subsystem for Linux (WSL) to run the scripts on the Windows machine. These are noted below.

## List of Scripts

The following is a list of the scripts in this repository:

### Specific Package Update Script (Linux)

This script updates specifically chosen packages on remote hosts. More information can be found in the [Specific Package Update README](update/system/linux/specific-package-update/README.md).

### Etherwake Script (Linux)

This script uses the `etherwake` command on a Linux machine to wake up a Windows machine on the network. More information can be found in the [Etherwake README](update/system/linux/etherwake/README.md).

### Ara Repository Pull Script (Linux / MacOS / Windows via WSL)

This script synchronises the [Ara](https://gitlab.com/conorjwryan/ara) git repository across remote hosts. More information can be found in the [Git Ara Pull README](update/git/ara/README.md).

### Ara Docker Update Script (Linux / MacOS / Windows via WSL)

This script updates the [Ara](https://gitlab.com/conorjwryan/ara) repository and docker containers across remote hosts. More information can be found in the [Ara Docker Update README](update/docker/ara/README.md).

### Scripts Repository Pull Script (Linux / MacOS / Windows via WSL)

This script synchronises the [Scripts](https://gitlab.com/conorjwryan/scripts) git repository across remote hosts. More information can be found in the [Git Scripts Pull README](update/git/scripts/README.md).

### Portainer Updater Script (Linux / MacOS / Windows via WSL)

This script updates the [Portainer](https://www.portainer.io/) docker containers across remote hosts. More information can be found in the [Portainer Updater README](update/docker/portainer/README.md).

### Windows Shutdown Script (Windows via WSL)

This script uses to remotely shutdown a `Windows` machine on the network using `WSL`. More information can be found in the [Windows Shutdown README](update/system/windows/shutdown/README.md).

## How to Setup and Run the Scripts

Clone the repository using the following command:

```bash
git clone https://gitlab.com/conorjwryan/ansible.git
```

Then change into the directory:

```bash
cd ansible
```

After you have cloned the repository you will need to create a hosts file. The hosts file is used to define the servers/computers you want to connect to with these automated scripts, stating both individual variables and groups which you set the scripts up so they run on many computers at once.

Each individual script in this repository has an accompanying `README.md` about how to set up the hosts file with the correct variables and run the chosen script but you can also use the `example.inventory.ini` file as a template.

```bash
# copy the example inventory file to hosts
cp example.inventory.ini hosts
# edit the hosts file to include your hosts and variables
nano hosts
```

### Notes About Sudo / Root

Some of the scripts require the use of sudo / root in order to perform tasks. For ease of use and understanding the `ansible_become_password` for each host is located in the `hosts`/`inventory` file. This however means that the passwords are stored in plain text which is not secure. For more information about this please see the [Notes About Encryption](#notes-about-encryption) section.

### Running the Scripts

After you have created the hosts file you can run the scripts using the following command:

```bash
ansible-playbook -i hosts update/docker/portainer/portainer-update.yml
```

## Notes About Encryption

Some of the scripts in this repository require the use of sudo / root in order to perform tasks. For ease of use and understanding the `ansible_become_password` for each host is located in the `hosts`/`inventory` file. This however means that the passwords are stored in plain text which is not secure.

Ansible comes installed with `ansible-vault` which allows you to encrypt specific files and variables. I recommend that after you have set up your hosts file you encrypt it using the following command:

```bash
ansible-vault encrypt hosts
```

It will then ask you to enter and confirm a password. This password will be required to run the playbooks. You can then run the playbooks using the following command:

```bash
ansible-playbook --ask-vault-pass -i hosts update/docker/portainer/portainer-update.yml
```

After the playbook is encrypted you will not be able to edit it using a IDE or text editor unless you decrypt it first. An alternative is to use the `ansible-vault edit` command.

To edit the hosts file you can use the following command:

```bash
ansible-vault edit hosts
```

The `ansible-vault` command has many other uses which can be found in the [Ansible Documentation](https://docs.ansible.com/ansible/latest/user_guide/vault.html).

### Note About Extra Variables

Some of the scripts require extra variables to be passed to them. These variables are passed using the `--extra-vars` flag in the `ansible-playbook` command. For example:

```bash
ansible-playbook --ask-vault-pass -i hosts update/system/windows/shutdown/shutdown.yml --extra-vars="HOST=example_computer_1"
```
